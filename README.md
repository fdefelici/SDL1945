#DA CAPIRE
> sdl_blitsurface vs sdl_rendercopy
> SDL_SetVideoMode vs SDL_CreateWindow

# Per ricevere eventi
https://www.libsdl.org/release/SDL-1.2.15/docs/html/guideinputkeyboard.html

# Per controllare movimento giocatore 
https://wiki.libsdl.org/SDL_GetKeyboardState

# SDL_Image example
http://gigi.nullneuron.net/gigilabs/loading-images-in-sdl2-with-sdl_image/



http://ict.loreto.qld.edu.au/year9/T3_Gamemaker/Scrolling_Shooter_Part1/page01.html
http://stephenmeier.net/2014/08/10/sdl-2-0-tutorial-00-the-basic-sturcture/
https://gamedev.stackexchange.com/questions/129269/game-loop-getting-58-62-fps-why-not-exactly-60fps-sdl-c-osx
https://interhacker.wordpress.com/2012/08/26/chapter-3-the-sdl-event-loop/
http://headerphile.com/sdl2/sdl2-part-2-your-first-sdl2-application/
# Verifca del fps minimo
https://labs.deadbodyoutline.com/2014/05/28/starting-a-sdl2-game-build-system-and-main-loop/
http://gameprogrammingpatterns.com/game-loop.html


#TODO:
* Splash Screen:
* Suoni: Background Music (mp3)
* Suoni: Effetto sparo (wav)
* Suoni: Effetto distruzione (wav)
* Sparo nemico
* Game Over a fine vite
* Gestione eccezzioni
 3. Dimensione finestra target
10. Migliorare Random (isole, nemici, etc..)
11. Concetto pooling: gestione piu istance dello stesso oggetto (isole, nemici)
* Implementare GameLoop avanzato
* Gestire collisioni con una lista (prima le trovo tutte e poi le invoco)
13. Gestione collisioni: introduzione layer?!?

#DONE:
* Gestione keyboard (con key multiple)
* Centrare finestra
* Gestione Sprite
* Far partire aereo da posizione centrata
* Aggiungere isole randomiche che simulano scrolling verticale
* Gestione collisioni: se non abilitato oggetto -> skip
* Aggiungere 1 un nemico randomico (senza sparo)
* Distruzione nemico se colpito da player: Collisione
* Distruzione nemico se colpito da sparo
* Aggiungere sparo player
* Refactoring: separare engine da app
* Distruggere player se colpito da sparo nemico
* Distruggere player se colpito da aereo
* Gestione UI (Energia player)
* Gestione UI (Vite player)
* Gestione UI (Punteggio)
