clang -o main.exe \
	-I./include -I./SDL2/include -I./SDL2_Image/include -I./SDL2_TTF/include -I../aiv_module/include \
	src/*.c ../aiv_module/src/*.c \
	-L./SDL2/lib/x64 -L./SDL2_Image/lib -L./SDL2_TTF/lib\
	-lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf