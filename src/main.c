#define SDL_MAIN_HANDLED
#include "SDL.h"
#include <eng.h>
#include <aiv/list.h>
#include "behav_backgr.h"
#include "behav_player.h"
#include "behav_enemy_mgr.h"

int main(int argc, char **argv) {
    XEngine eng;
    x_init(&eng);

    eng.objs->add(eng.objs, new_MyBackgr());
    
    eng.objs->add(eng.objs, new_MyPlayer());
    eng.objs->add(eng.objs, new_MyEnemyMgr());

    eng.objs->add(eng.objs, new_MyUIMgr()); //COSI LA UI STA SOPRA.
    x_run(&eng);
    return 0;
}
