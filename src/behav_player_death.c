#include <eng.h>
#include <aiv/list.h>
#include "props.h"
#include "behav_player.h"
#include "behav_backgr.h"

static void start();
static void update();

XBehaviour* new_MyPlayerDeathBehav() {
    XBehaviour *behav = new_XBehavior();
    behav->start = start;
    behav->update = update;
    return behav;
}

static void start(XObject *self, XEngine *eng) {
    //printf("START: %s\n", self->name);    
    XObject *explosion = new_MyEnemyExplosion("PlayerExplos", "res/assets/explosion2_strip7.png", 7);
    explosion->enabled = PROPS__FALSE;
    //sprite->speed = PROPS__SPRITE_TRANSITION_SPEED;

    XObject *player = self;
    aiv_list_add(player->children, explosion);


    MyPlayer *data = (MyPlayer *)self->custom;
    data->explosion = explosion;
    //data->uiMgr = XObject_findByName(eng, "UIMgr");

}

static void update(XObject *self, XEngine *eng) {
    MyPlayer *data = (MyPlayer *)self->custom;
    if (data->vital > 0) return;

    data->lifes--; if (data->lifes < 0) data->lifes = 0;

    if (data->lifes > 0) {
        data->explosion->transform.x = self->transform.x;
        data->explosion->transform.y = self->transform.y;
        data->explosion->enabled = PROPS__TRUE;
        data->vital = PROPS__PLAYER_VITAL;
        
        data->disabled = PROPS__TRUE;

        uiMgr_playerDeath(data->uiMgr);
        uiMgr_playerHit(data->uiMgr, (data->vital*100)/PROPS__PLAYER_VITAL);
    } else {
        uiMgr_playerDeath(data->uiMgr);
        printf("GAME OVER\n");
    }

}
