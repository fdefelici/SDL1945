#include <eng.h>
#include <aiv/list.h>
#include "props.h"
#include <stdio.h>
#include <string.h>

//TODO COME FARE TYPEDEF STATICO?
typedef struct {
    XImage image;
} CustomData;


static void start();
static void update();
static void destroy();
static void onCollision();

XObject* new_MyPlayerBullet(char *name) {
    XObject *obj = new_XObject(name);
    obj->enabled = PROPS__FALSE;
    obj->colliderEnabled = PROPS__TRUE;

    XBehaviour *behav = new_XBehavior();
    behav->start = start;
    behav->update = update;
    behav->destroy = destroy;
    behav->onCollision = onCollision;
    aiv_list_add(obj->behaviours, behav);

    CustomData *data = calloc(1, sizeof(CustomData));
    obj->custom = data;
    return obj; 
}

static void start(XObject *self, XEngine *eng) {
    CustomData *data = (CustomData *)self->custom;
    XImage *image = &(data->image);
    XImage_init(eng, image, "res/assets/bullet.png");

    self->transform.x = 0;
    self->transform.y = 0;    
    self->transform.w = image->width;
    self->transform.h = image->height;    
}

static void update(XObject *self, XEngine *eng) {
    CustomData *data = (CustomData *)self->custom;
    XImage *image = &(data->image);
    
    self->transform.y -= PROPS__PLAYER_SHOOT_SPEED * eng->deltaTime;
    if (self->transform.y < 0) {
        self->enabled = PROPS__FALSE;
    }

    XImage_update(eng, image, self);
}

static void destroy(XObject *self, XEngine *eng) {
    XObject_destroy(self);
}

static void onCollision(XObject *self, XObject *other) {
    if (!strcmp(other->name, "Player")) return;
    self->enabled = PROPS__FALSE;
    
    //TODO: DA NON FARE...
    //Nota: coviene collezionare prima tutti le collisioni e poi eseguirle:
    XBehaviour *b = (XBehaviour *)aiv_list_at(other->behaviours, 0);
    b->onCollision(other, self);
}
