#include "SDL.h"
#include "SDL2/SDL_image.h"
#include <stdio.h>
#include <eng.h>
#include <string.h>
#include "props.h"

float clamp(float value, float min, float max) {
    if (value < min) return 0;
    if (value > max) return max;
    return value;
}

int xrand(int min, int max) {
    return min + rand() % (max+1 - min);
}


XObject* new_XObject(char *name) {
    XObject *ptr = calloc(1, sizeof(XObject));
    strcpy(ptr->name, name);
    ptr->children = aiv_list_new();
    ptr->behaviours = aiv_list_new();
    ptr->enabled = PROPS__TRUE;
    ptr->colliderEnabled = PROPS__FALSE;
    return ptr;
}

void XObject_fillRect(XObject *object, SDL_Rect *rect) {
    rect->x = object->transform.x;
    rect->y = object->transform.y;
    rect->w = object->transform.w;
    rect->h = object->transform.h;
}

//TODO: Fare recurs per ricercare nei figli. ora cerca solo al primo livello
static XObject* recursObj(XEngine *eng, aiv_list_t *objects, char *name) {
    //if (*result) return;
    //printf("%d\n", aiv_list_size(objects));
    for(int i=0; i<aiv_list_size(objects); i++) {
        XObject *each = (XObject*)(aiv_list_at(objects, i));       
        //printf("%s\n", each->name);
        if ( strcmp(each->name, name) == 0 ) {
            //printf("name: %s\n", name);
            return each;
        } 
    }
    return NULL;
    
}

XObject* XObject_findByName(XEngine *eng, char* objName) {
    //XObject **result;
    XObject *result = recursObj(eng, eng->objs, objName);
    return result;
}

void nullFunct() {}
XBehaviour* new_XBehavior() {
    XBehaviour *ptr = malloc(sizeof(XBehaviour));
    ptr->start = nullFunct;
    ptr->update = nullFunct;
    ptr->destroy = nullFunct;
    ptr->onCollision = nullFunct;
    return ptr;
}

void XImage_init(XEngine *eng, XImage *image, char *img_path) {
    SDL_Surface *surf = IMG_Load(img_path);
    image->texture = SDL_CreateTextureFromSurface(eng->renderer, surf);
    image->width = surf->w; 
    image->height = surf->h;
    strcpy(image->path, img_path);
    SDL_FreeSurface(surf);
}

void XImage_update(XEngine *eng, XImage *image, XObject *obj) {
    /*
    SDL_Rect srcRect;
    srcRect.x = 0;
    srcRect.y = 0;
    srcRect.w = obj->transform.w;
    srcRect.h = obj->transform.h;
    */
    
    SDL_Rect tileRect;
    tileRect.x = obj->transform.x;
    tileRect.y = obj->transform.y;
    tileRect.w = obj->transform.w;
    tileRect.h = obj->transform.h;
    SDL_RenderCopy(eng->renderer, image->texture, NULL, &tileRect);
}

void XSprite_init(XEngine *eng, XSprite *sprite, char *img_path, unsigned int numberOfImgs) {
    XImage *img = (XImage *)sprite;
    strcpy(img->path, img_path);
    
    SDL_Surface *myPlaneSurf = IMG_Load(img->path);
    
    img->texture = SDL_CreateTextureFromSurface(eng->renderer, myPlaneSurf);
    img->width = myPlaneSurf->w; 
    img->height = myPlaneSurf->h;
    SDL_FreeSurface(myPlaneSurf);

    sprite->number = numberOfImgs;
    sprite->width = img->width / sprite->number;
    sprite->height = img->height;
    sprite->index = 0;
    sprite->_pixelCounter = 0;
    sprite->cycles = -1; //INFINITO
}

void XSprite_update(XEngine *eng, XSprite *sprite, XObject *obj) {
    if (sprite->cycles != -1 && sprite->_cycleCounter >= sprite->cycles) {
        return;
    }
    sprite->_pixelCounter += (eng->deltaTime * (sprite->width * sprite->speed));
    if (sprite->_pixelCounter >= sprite->width) {
        sprite->_pixelCounter = 0;
        sprite->index++;
        sprite->index = sprite->index % sprite->number;
        if (sprite->index == 0) sprite->_cycleCounter++; //Se torno a indice 0 allora ho finito un ciclo
    }
        
    SDL_Rect srcRect;
    srcRect.x = sprite->index * sprite->width;
    srcRect.y = 0;
    srcRect.w = sprite->width;
    srcRect.h = sprite->height;

    SDL_Rect dstRect;
    dstRect.x = obj->transform.x;
    dstRect.y = obj->transform.y;
    dstRect.w = obj->transform.w;
    dstRect.h = obj->transform.h;

    SDL_RenderCopy(eng->renderer, sprite->base.texture, &srcRect, &dstRect);
}


//TODO: Migliorare il destroy: ciclare sugli elementi della lista e richiamare:
// - XObject_destroy sui child
// - XBehaviour_destroy sui behav
void XObject_destroy(XObject *self) {
    free(self->custom);
    aiv_list_wipe(self->behaviours);
    aiv_list_wipe(self->children);
    free(self);
}




static SDL_Window *win = NULL;
static SDL_Renderer *renderer = NULL;
static XEntity *player;

void x_init(XEngine *eng) {
    SDL_Init(SDL_INIT_VIDEO);
    win = SDL_CreateWindow("Hello World", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIN_WIDTH, WIN_HEIGHT, 0);
    renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);

    eng->deltaTime = 0;
    eng->fps = 0;
    eng->objs = aiv_list_new();
    eng->renderer = renderer;
}

static void recurs(XEngine *eng, aiv_list_t *objects, int who, void (*funct)(XEngine*, XObject*, XBehaviour*) ) {
    for(int i=0; i<aiv_list_size(objects); i++) {
        XObject *each = (XObject*)(aiv_list_at(objects, i));
        if (who==PROPS_OBJ_ALL ||  (who==PROPS_OBJ_ENABLED && each->enabled)) {
            for(int j=0; j<aiv_list_size(each->behaviours); j++) {
                XBehaviour *eachBehav = (XBehaviour*)(aiv_list_at(each->behaviours, j));
                funct(eng, each, eachBehav);    
            }
            recurs(eng, each->children, who, funct);
        }
    }
}

static void callStart(XEngine *eng, XObject *obj, XBehaviour *behav) {
    behav->start(obj, eng);    
}

static void callUpdate(XEngine *eng, XObject *obj, XBehaviour *behav) {
    behav->update(obj, eng);    
}

static void callDestroy(XEngine *eng, XObject *obj, XBehaviour *behav) {
    behav->destroy(obj, eng);    
}

void x_start(XEngine *eng) {
    recurs(eng, eng->objs, PROPS_OBJ_ALL, callStart);
}

static void checkCollRecurs2(XObject *each, XBehaviour *eachBehav, aiv_list_t *objs) {
     for(int k=0; k<aiv_list_size(objs); k++) {
        XObject *each2 = (XObject*)(aiv_list_at(objs, k));
        if (!each2->enabled) continue;

        if (each2->colliderEnabled && strcmp(each->name, each2->name)) { 
            SDL_Rect first; XObject_fillRect(each, &first);
            SDL_Rect second; XObject_fillRect(each2, &second);
            SDL_bool hasCollision = SDL_HasIntersection(&first, &second);
            if (hasCollision) { 
                eachBehav->onCollision(each, each2);
            }    
        }
        checkCollRecurs2(each, eachBehav, each2->children);
    }
}


static void checkCollisionRecus(XEngine *eng, aiv_list_t* objs) {
    for(int i=0; i<aiv_list_size(objs); i++) {
        XObject *each = (XObject*)(aiv_list_at(objs, i));
        if (!each->enabled) continue;

        if (each->colliderEnabled) {
            for(int j=0; j<aiv_list_size(each->behaviours); j++) {
                XBehaviour *eachBehav = (XBehaviour*)(aiv_list_at(each->behaviours, j));
                if (!eachBehav->onCollision) continue;
                checkCollRecurs2(each, eachBehav, eng->objs);
            }
        }
        checkCollisionRecus(eng, each->children);
    }
}


void x_update(XEngine *eng) {
    SDL_RenderClear(renderer);

    checkCollisionRecus(eng, eng->objs);
    recurs(eng, eng->objs, PROPS_OBJ_ENABLED, callUpdate);

    SDL_RenderPresent(renderer);
}

void x_destroy(XEngine *eng) {
    //recurs(eng, eng->objs, callDestroy);

    //distruggere la lista di entities
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(win);
}

int x_run(XEngine *eng) {
    x_start(eng);
    
    float delta_time = 0;
    unsigned long long last = 0;
    unsigned long long now = SDL_GetPerformanceCounter();
    unsigned int quit = 0;
    while (!quit) {
        SDL_Event event;
        if (SDL_PollEvent(&event)) {
            switch( event.type ) {
                case SDL_QUIT:
                    quit = 1;
                    break;
                default:
                    break;
            }
        }
        x_update(eng);
        

        last = now;
        now = SDL_GetPerformanceCounter();                       //=>"Returns current counter value"
        unsigned long long freq = SDL_GetPerformanceFrequency(); //=>"Returns a platform-specific count per second"
        delta_time = (float)(now-last) / (float)freq ;
        float fps = (float)1 / delta_time;
        
        eng->deltaTime = delta_time;
        eng->fps = fps;
        //printf("delta: %f fps: %f\n", delta_time, fps);
        
    }

    x_destroy(eng);

    SDL_Quit();
    return 0;
}
