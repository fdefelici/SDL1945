#include <eng.h>
#include <aiv/list.h>
#include "props.h"
#include "SDL2/SDL_image.h"
#include <string.h>

typedef struct {
    XImage image;
} CustomData;

static void start();
static void update();
static void destroy();

XObject* new_MyIsland(char *name, char *path) {
    XObject *obj = new_XObject(name); 
    
    XBehaviour *behav = new_XBehavior();
    behav->start = start;
    behav->update = update;
    behav->destroy = destroy;

    aiv_list_add(obj->behaviours, behav);
    
    CustomData *instance = calloc(1, sizeof(CustomData));
    strcpy(instance->image.path, path);
    obj->custom = instance;

    return obj;   
}

static void start(XObject *self, XEngine *eng) {
    CustomData *data = (CustomData*)self->custom;
    XImage *image = &data->image;
    XImage_init(eng, image, image->path);
    
    self->transform.x = xrand(0, WIN_WIDTH - image->width); 
    self->transform.y = xrand(-WIN_HEIGHT, WIN_HEIGHT / 2); 
    self->transform.w = image->width; 
    self->transform.h = image->height;
}

static void update(XObject *self, XEngine *eng) {
    CustomData *data = (CustomData*)self->custom;
    XImage *image = &data->image;
    self->transform.y += PROPS__BG_SCROLL_SPEED * eng->deltaTime;
    if (self->transform.y >= WIN_HEIGHT) {
        self->transform.x = xrand(0, WIN_WIDTH - image->width); 
        self->transform.y = xrand(-WIN_HEIGHT * 2, -WIN_HEIGHT / 2); 
    }
    XImage_update(eng, image, self);
}

static void destroy(XObject *self, XEngine *eng) {
    XObject_destroy(self);
}
