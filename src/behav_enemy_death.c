#include <eng.h>
#include <aiv/list.h>
#include "props.h"
#include <stdio.h>
#include <string.h>

//TODO COME FARE TYPEDEF STATICO?
typedef struct {
    XSprite sprite;
    char tmp_path[256];  //Variabili di appoggio temporanee;
    unsigned int tmp_number;
    int tmp_cycles;
} CustomData;


static void start();
static void update();
static void destroy();

XObject* new_MyEnemyExplosion(char *name, char *spritePath, unsigned int spriteNumber) {
    XObject *obj = new_XObject(name); 

    XBehaviour *behav = new_XBehavior();
    behav->start = start;
    behav->update = update;
    behav->destroy = destroy;
    aiv_list_add(obj->behaviours, behav);

    CustomData *data = calloc(1, sizeof(CustomData));
    strcpy(data->tmp_path, spritePath);
    data->tmp_number = spriteNumber;
    data->tmp_cycles = 1;

    obj->custom = data;
    return obj; 
}

static void start(XObject *self, XEngine *eng) {
    CustomData *data = (CustomData *)self->custom;
    XSprite *sprite = &(data->sprite);
    XSprite_init(eng, sprite, data->tmp_path, data->tmp_number);
    sprite->speed = PROPS__SPRITE_TRANSITION_SPEED;
    sprite->cycles = data->tmp_cycles;
    
    self->transform.x = 0;
    self->transform.y = 0;    
    self->transform.w = sprite->width;
    self->transform.h = sprite->height;    
}

static void update(XObject *self, XEngine *eng) {
    CustomData *data = (CustomData *)self->custom;
    XSprite *sprite = &(data->sprite);
    if (sprite->cycles != -1 && sprite->_cycleCounter >= sprite->cycles) {
        self->enabled = PROPS__FALSE;
        sprite->_cycleCounter = 0;
        sprite->index = 0;
        sprite->_pixelCounter = 0;
        return;
    }
    XSprite_update(eng, sprite, self);
}

static void destroy(XObject *self, XEngine *eng) {
    XObject_destroy(self);
}
