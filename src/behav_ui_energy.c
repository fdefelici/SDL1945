#include <eng.h>
#include <aiv/list.h>
#include "props.h"
#include "SDL2/SDL_image.h"
#include <string.h>

typedef struct {
    XImage image;
} CustomData;

static void start();
static void update();
static void destroy();

XObject* new_MyUIEnergy() {
    XObject *obj = new_XObject("energy"); 
    
    XBehaviour *behav = new_XBehavior();
    behav->start = start;
    behav->update = update;
    behav->destroy = destroy;
    aiv_list_add(obj->behaviours, behav);

    CustomData *instance = calloc(1, sizeof(CustomData));
    obj->custom = instance;
    return obj;   
}

static void start(XObject *self, XEngine *eng) {
    CustomData *data = (CustomData*)self->custom;
    XImage *image = &data->image;
    XImage_init(eng, image, "res/assets/energy.png");
    
    /* Altrimenti sovrascrive i setting di UI
    self->transform.x = 0; 
    self->transform.y = 0; 
    self->transform.w = image->width; 
    self->transform.h = image->height;
    */
}

static void update(XObject *self, XEngine *eng) {
    CustomData *data = (CustomData*)self->custom;
    XImage *image = &data->image;
    XImage_update(eng, image, self);
}

static void destroy(XObject *self, XEngine *eng) {
    XObject_destroy(self);
}
