#include <eng.h>
#include <aiv/list.h>
#include "props.h"
#include "behav_enemy.h"

/*
typedef struct {
    XSprite sprite;
} MyPlayer;
*/

static void start();
static void update();
static void destroy();

XObject* new_MyEnemyMgr() {
    XObject *obj = new_XObject("EnemyMgr"); 
    
    XBehaviour *behav = new_XBehavior();
    behav->start = start;
    behav->update = update;
    behav->destroy = destroy;
    aiv_list_add(obj->behaviours, behav);

    XObject *enemy1 = new_MyEnemy("enemy1","res/assets/enemy1_strip3.png", 3);
    aiv_list_add(obj->children, enemy1);

    XObject *enemy2 = new_MyEnemy("enemy2","res/assets/enemy2_strip3.png", 3);
    aiv_list_add(obj->children, enemy2);

    XObject *enemy3 = new_MyEnemy("enemy3","res/assets/enemy3_strip3.png", 3);
    aiv_list_add(obj->children, enemy3);
    return obj;   
}

static void start(XObject *self, XEngine *eng) {
    //Qui non chiama lo start sul figlio. E rischio di andare in contesa sulla lista;
    //XObject *enemy1 = new_XObject("enemy1");
    //aiv_list_add(self->children, enemy1);
}

static void update(XObject *self, XEngine *eng) {
}

static void destroy(XObject *self, XEngine *eng) {
    XObject_destroy(self);
}
