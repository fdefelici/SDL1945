#include <eng.h>
#include <aiv/list.h>
#include "props.h"
#include <stdio.h>
#include "behav_backgr.h"
#include <string.h>

typedef struct {
    XSprite sprite;
    char tmp_path[256];  //Variabili di appoggio temporanee;
    unsigned int tmp_number;
    XObject *explosion;
    XObject *player;
} MyEnemy;


static void start();
static void update();
static void destroy();
static void onCollision();

//XObject* new_MyEnemy(char *name, char *spritePath, unsigned int spriteNumber) {
XObject* new_MyEnemy(char *name, char *spritePath, unsigned int spriteNumber) {
    XObject *obj = new_XObject(name); 
    obj->colliderEnabled = PROPS__TRUE;

    XBehaviour *behav = new_XBehavior();
    behav->start = start;
    behav->update = update;
    behav->destroy = destroy;
    behav->onCollision = onCollision;
    aiv_list_add(obj->behaviours, behav);

    XObject *explosion = new_MyEnemyExplosion("expl1", "res/assets/explosion1_strip6.png", 6);
    explosion->enabled = PROPS__FALSE;
    aiv_list_add(obj->children, explosion);

    MyEnemy *instance = calloc(1, sizeof(MyEnemy));
    strcpy(instance->tmp_path, spritePath);
    instance->tmp_number = spriteNumber;
    instance->explosion = explosion;
    obj->custom = instance;
    return obj; 
}

static void start(XObject *self, XEngine *eng) {
    MyEnemy *data = (MyEnemy *)self->custom;
    XSprite *sprite = &(data->sprite);
    XSprite_init(eng, sprite, data->tmp_path, data->tmp_number);
    sprite->speed = PROPS__SPRITE_TRANSITION_SPEED;
    
    //POSIZIONE INIZIALE SULLA MAPPA;
    self->transform.x = xrand(0, WIN_WIDTH - sprite->width);
    self->transform.y = xrand( -WIN_HEIGHT, -WIN_HEIGHT / 2);
    self->transform.w = sprite->width;
    self->transform.h = sprite->height;

    data->player = XObject_findByName(eng, "Player");
}

static void update(XObject *self, XEngine *eng) {
    self->transform.y += PROPS__ENEMY_MOVE_SPEED * eng->deltaTime;
    if (self->transform.y > WIN_HEIGHT) {
        self->transform.x = xrand(0, WIN_WIDTH - self->transform.w);
        self->transform.y = xrand(-WIN_HEIGHT, -WIN_HEIGHT / 2);
    }

    MyEnemy *data = (MyEnemy *)self->custom;
    XSprite_update(eng, &(data->sprite), self);
}

static void destroy(XObject *self, XEngine *eng) {
    XObject_destroy(self);
}

static void onCollision(XObject *self, XObject *other) {
    MyEnemy *data = (MyEnemy *)self->custom;
    data->explosion->transform.x = self->transform.x;
    data->explosion->transform.y = self->transform.y;
    data->explosion->enabled = PROPS__TRUE;
    
    self->transform.x = xrand(0, WIN_WIDTH - self->transform.w);
    self->transform.y = xrand(-WIN_HEIGHT, -WIN_HEIGHT / 2);

    player_score(data->player, 20);
}
