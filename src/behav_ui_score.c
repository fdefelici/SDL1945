#include <eng.h>
#include <aiv/list.h>
#include "props.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_ttf.h"
#include <string.h>

typedef struct {
    XText text;
    char value[6];
} CustomData;

static void start();
static void update();
static void destroy();

void uiScore_value(XObject *self, char *value) {
    CustomData *data = (CustomData *)self->custom;
    strcpy(data->value, value);
}

XObject* new_MyUIScore() {
    XObject *obj = new_XObject("score"); 
    
    XBehaviour *behav = new_XBehavior();
    behav->start = start;
    behav->update = update;
    behav->destroy = destroy;
    aiv_list_add(obj->behaviours, behav);

    CustomData *instance = calloc(1, sizeof(CustomData));
    strcpy(instance->value, "0");
    obj->custom = instance;
    return obj;   
}

static void start(XObject *self, XEngine *eng) {
    
    //CustomData *data = (CustomData*)self->custom;
    
    
    /* Altrimenti sovrascrive i setting di UI
    self->transform.x = 0; 
    self->transform.y = 0; 
    self->transform.w = image->width; 
    self->transform.h = image->height;
    */
}

static void update(XObject *self, XEngine *eng) {
    CustomData *data = (CustomData*)self->custom;
    
    if(!TTF_WasInit() && TTF_Init()==-1) {
        printf("TTF_Init: %s\n", TTF_GetError());
        exit(2);
    }
    TTF_Font* Sans = TTF_OpenFont("res/fonts/open-sans/OpenSans-Bold.ttf", 18); //this opens a font style and sets a size
    if (!Sans) {
        printf("%s\n", TTF_GetError());
    }

    SDL_Color White = {255, 255, 0};  // this is the color in rgb format, maxing out all would give you the color white, and it will be your text's color
    SDL_Surface* surf = TTF_RenderText_Solid(Sans, data->value, White); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first
    SDL_Texture* Message = SDL_CreateTextureFromSurface(eng->renderer, surf); //now you can convert it into a texture

    SDL_Rect Message_rect; 
    Message_rect.x = 224 - surf->w;  
    Message_rect.y = WIN_HEIGHT - 40; 
    Message_rect.w = surf->w; 
    Message_rect.h = surf->h; 

    SDL_RenderCopy(eng->renderer, Message, NULL, &Message_rect); //
}

static void destroy(XObject *self, XEngine *eng) {
    XObject_destroy(self);
}
