#include <eng.h>
#include <aiv/list.h>
#include "props.h"
#include "SDL2/SDL_image.h"
#include "behav_island.h"

typedef struct {
    XImage tile;
    float scroll_offset;
} MyBackgr;

static void start();
static void update();
static void destroy();

XObject* new_MyBackgr() {
    XObject *obj = new_XObject("MyBackgr"); 
    
    XBehaviour *behav = new_XBehavior();
    behav->start = start;
    behav->update = update;
    behav->destroy = destroy;
    aiv_list_add(obj->behaviours, behav);

    XObject *island1 = new_MyIsland("island1", "res/assets/island1.png");
    XObject *island11 = new_MyIsland("island1", "res/assets/island1.png");
    XObject *island2 = new_MyIsland("island2", "res/assets/island2.png");
    XObject *island3 = new_MyIsland("island3", "res/assets/island3.png");
    XObject *island31 = new_MyIsland("island31", "res/assets/island3.png");
    XObject *island32 = new_MyIsland("island32", "res/assets/island3.png");
    XObject *island33 = new_MyIsland("island32", "res/assets/island3.png");
    aiv_list_add(obj->children, island1);
    aiv_list_add(obj->children, island11);
    aiv_list_add(obj->children, island2);
    aiv_list_add(obj->children, island3);
    aiv_list_add(obj->children, island31);
    aiv_list_add(obj->children, island33);
    
    MyBackgr *instance = calloc(1, sizeof(MyBackgr));
    obj->custom = instance;

    return obj;   
}

static void start(XObject *self, XEngine *eng) {
    MyBackgr *data = (MyBackgr*)self->custom;
    
    //XImage_init(eng, "res/assets/water.png");
    SDL_Surface *surf = IMG_Load("res/assets/water.png");
    data->tile.texture = SDL_CreateTextureFromSurface(eng->renderer, surf);
    data->tile.width = surf->w; 
    data->tile.height = surf->h;
    data->scroll_offset = 0;

    self->transform.x = 0; 
    self->transform.y = 0;
    self->transform.w = WIN_WIDTH; 
    self->transform.h = WIN_HEIGHT; 

    SDL_FreeSurface(surf);
}

static void update(XObject *self, XEngine *eng) {
    MyBackgr *data = (MyBackgr*)self->custom;

    int hTiles = WIN_WIDTH / data->tile.width;
    int vTiles = WIN_HEIGHT / data->tile.height;

    data->scroll_offset += PROPS__BG_SCROLL_SPEED * eng->deltaTime;
    if (data->scroll_offset >= data->tile.height) data->scroll_offset = 0;
    unsigned int offset_int = data->scroll_offset;

    for(int i=0; i < hTiles; i++) {
        for(int j=0; j < vTiles; j++) {
            if (offset_int > 0 && j == 0)  {
                SDL_Rect tileRect;
                tileRect.x = i * data->tile.width;
                tileRect.y = j * data->tile.height - (data->tile.height - offset_int);
                tileRect.w = data->tile.width;
                tileRect.h = data->tile.height;
                SDL_RenderCopy(eng->renderer, data->tile.texture, NULL, &tileRect);    
            }

            SDL_Rect tileRect;
            tileRect.x = i * data->tile.width;
            tileRect.y = j * data->tile.height + offset_int;
            tileRect.w = data->tile.width;
            tileRect.h = data->tile.height;
            SDL_RenderCopy(eng->renderer, data->tile.texture, NULL, &tileRect);
        }    
    }
}

static void destroy(XObject *self, XEngine *eng) {
    XObject_destroy(self);
}
