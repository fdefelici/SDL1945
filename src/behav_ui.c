#include <eng.h>
#include <aiv/list.h>
#include "props.h"
#include "SDL2/SDL_image.h"
#include <string.h>
#include "behav_backgr.h"
#include <stdio.h>

typedef struct {
    XImage image;
    XObject *energy;
    //void (*playerHit)();
    XObject *life1;
    XObject *life2;
    XObject *life3;
    unsigned int lifeIndex;
    XObject *score;
} CustomData;

static void start();
static void update();
static void destroy();

void uiMgr_playerHit(XObject *self, unsigned int currentVital) {
    CustomData *instance = (CustomData*)self->custom;
    unsigned int width = (currentVital * PROPS__UI_ENERGY_MAX_PIXELS) / 100;
    instance->energy->transform.w = width;
}

void uiMgr_playerDeath(XObject *self) {
    CustomData *data = (CustomData*)self->custom;
    if (data->lifeIndex == 0) {
        data->life1->enabled = PROPS__FALSE;
    } else if (data->lifeIndex == 1) {
        data->life2->enabled = PROPS__FALSE;
    } else if (data->lifeIndex == 2) {
        data->life3->enabled = PROPS__FALSE;
    }
    data->lifeIndex++;
}

void uiMgr_score(XObject *self, int score) {
   CustomData *data = (CustomData*)self->custom;
   char score_value[6];
   sprintf(score_value, "%d", score);
   uiScore_value(data->score, score_value);
}

XObject* new_MyUIMgr() {
    XObject *obj = new_XObject("UIMgr"); 
    
    XBehaviour *behav = new_XBehavior();
    behav->start = start;
    behav->update = update;
    behav->destroy = destroy;
    aiv_list_add(obj->behaviours, behav);

    XObject *energy = new_MyUIEnergy();
    aiv_list_add(obj->children, energy);

    XObject *life1 = new_MyUILife("life1");
    XObject *life2 = new_MyUILife("life2");
    XObject *life3 = new_MyUILife("life3");
    aiv_list_add(obj->children, life1);
    aiv_list_add(obj->children, life2);
    aiv_list_add(obj->children, life3);

    XObject *score = new_MyUIScore();
    aiv_list_add(obj->children, score);

    CustomData *data = calloc(1, sizeof(CustomData));
    data->energy = energy;
    data->life1 = life1;
    data->life2 = life2;
    data->life3 = life3;
    data->score = score;

    obj->custom = data;

    return obj;   
}

static void start(XObject *self, XEngine *eng) {
    CustomData *data = (CustomData*)self->custom;
    
    XImage *image = &data->image;
    XImage_init(eng, image, "res/assets/bottom.png");
    self->transform.x = 0; 
    self->transform.y = WIN_HEIGHT - image->height; 
    self->transform.w = image->width; 
    self->transform.h = image->height;

    XObject *life1 = data->life1;
    life1->transform.x = 30;
    life1->transform.y = WIN_HEIGHT - 70;
    
    XObject *life2 = data->life2;
    life2->transform.x = 58;
    life2->transform.y = WIN_HEIGHT - 70;

    XObject *life3 = data->life3;
    life3->transform.x = 86;
    life3->transform.y = WIN_HEIGHT - 70;

    XObject *energy = data->energy;
    energy->transform.x = 12;
    energy->transform.y = WIN_HEIGHT - 31;
    energy->transform.w = PROPS__UI_ENERGY_MAX_PIXELS;
    energy->transform.h = 10;
}

static void update(XObject *self, XEngine *eng) {
    CustomData *data = (CustomData*)self->custom;
    XImage *image = &data->image;
    /*
    self->transform.y += PROPS__BG_SCROLL_SPEED * eng->deltaTime;
    if (self->transform.y >= WIN_HEIGHT) {
        self->transform.x = xrand(0, WIN_WIDTH - image->width); 
        self->transform.y = xrand(-WIN_HEIGHT * 2, -WIN_HEIGHT / 2); 
    }
    */
    XImage_update(eng, image, self);
}

static void destroy(XObject *self, XEngine *eng) {
    XObject_destroy(self);
}
