#include <eng.h>
#include <aiv/list.h>
#include "props.h"
#include "behav_player_shoot.h"
#include "behav_backgr.h"
#include "behav_player.h"

static void start();
static void update();
static void destroy();
static void onCollision();

static XBehaviour* prova() {
    return new_XBehavior();
}

XObject* new_MyPlayer() {
    XObject *obj = new_XObject("Player"); 
    obj->colliderEnabled = PROPS__TRUE;

    XBehaviour *behav = new_XBehavior();
    behav->start = start;
    behav->update = update;
    behav->destroy = destroy;
    behav->onCollision = onCollision;
    aiv_list_add(obj->behaviours, behav);

    aiv_list_add(obj->behaviours, new_MyPlayerDeathBehav());

    XObject *bullet = new_MyPlayerBullet("bullet1");
    bullet->enabled = PROPS__FALSE;
    aiv_list_add(obj->children, bullet);
/*
    XObject *explosion = new_MyEnemyExplosion("PlayerExplos", "res/assets/explosion2_strip7.png", 7);
    explosion->enabled = PROPS__FALSE;
    aiv_list_add(obj->children, explosion);
*/
    MyPlayer *instance = calloc(1, sizeof(MyPlayer));
    instance->bullet = bullet;
    instance->vital = PROPS__PLAYER_VITAL;
    instance->lifes = PROPS__PLAYER_LIFES;
    obj->custom = instance;
    return obj;   
}

static void start(XObject *self, XEngine *eng) {
    MyPlayer *player = (MyPlayer *)self->custom;
    XSprite *sprite = &(player->sprite);
    XSprite_init(eng, sprite, "res/assets/myplane_strip3.png", 3);
    sprite->speed = PROPS__SPRITE_TRANSITION_SPEED;
    
    //POSIZIONE INIZIALE SULLA MAPPA;
    int centerX = WIN_WIDTH / 2 - sprite->width / 2;
    int centerY = WIN_HEIGHT / 2 - sprite->height / 2;
    self->transform.x = centerX;
    self->transform.y = centerY;
    self->transform.w = sprite->width;
    self->transform.h = sprite->height;

    XObject *uiMgr = XObject_findByName(eng, "UIMgr");
    player->uiMgr = uiMgr;
}

static void _move_by_keys(XEngine *eng, XObject *obj) {
    const Uint8 *keyStates = SDL_GetKeyboardState(NULL);
    float pixelMove = PROPS__PLAYER_MOVE_SPEED * eng->deltaTime;
    float xpos = obj->transform.x;
    float ypos = obj->transform.y;
    if (keyStates[SDL_SCANCODE_LEFT]) {
        xpos -= pixelMove;
        obj->transform.x = clamp(xpos, 0, WIN_WIDTH - obj->transform.w);
    }
    if (keyStates[SDL_SCANCODE_RIGHT]) {
        xpos += pixelMove;
        obj->transform.x = clamp(xpos, 0, WIN_WIDTH - obj->transform.w);
    }
    if (keyStates[SDL_SCANCODE_UP]) {
        ypos -= pixelMove;
        obj->transform.y = clamp(ypos, 0, WIN_HEIGHT - obj->transform.h);
    }
    if (keyStates[SDL_SCANCODE_DOWN]) {
        ypos += pixelMove;
        obj->transform.y = clamp(ypos, 0, WIN_HEIGHT - obj->transform.h);
    }
}


static void update(XObject *self, XEngine *eng) {
    MyPlayer *data = (MyPlayer *)self->custom;
    if (data->disabled) {
        self->colliderEnabled = PROPS__FALSE;
        data->disabledTimer += eng->deltaTime;
        if (data->disabledTimer < 1) return;

        self->transform.x = WIN_WIDTH / 2 - self->transform.w / 2;
        self->transform.y = WIN_HEIGHT / 2 - self->transform.h / 2;

        data->blinkTimer += eng->deltaTime;
        float seed = data->blinkTimer * 5;
        int index = ((int)seed) % 2;
        if (data->blinkTimer < 3) {   
            if (index == 0) {
                XSprite_update(eng, &(data->sprite), self);
            } 
            return;
        }

        self->colliderEnabled = PROPS__TRUE;
        data->disabled = PROPS__FALSE;
        data->disabledTimer = 0;
        data->blinkTimer = 0;
        return;
    }
    
    _move_by_keys(eng, self);

    const Uint8 *keyStates = SDL_GetKeyboardState(NULL);
    if (keyStates[SDL_SCANCODE_SPACE] && !data->bullet->enabled) {
        data->bullet->transform.x = self->transform.x + self->transform.w / 4;
        data->bullet->transform.y = self->transform.y - self->transform.h / 4;
        data->bullet->enabled = PROPS__TRUE;
    }
    XSprite_update(eng, &(data->sprite), self);
}

static void destroy(XObject *self, XEngine *eng) {
    XObject_destroy(self);
}

//TODO: Creare struttura collision con punto di collisione
static void onCollision(XObject *self, XObject *other) {
    //TODO: Aggiungere TAG agli oggetti cosi da ingnorare per tag
    if (strcmp(other->name, "bullet1") == 0 ) return; 

    MyPlayer *data = (MyPlayer *)self->custom;

    int damage = 30;
    
    data->vital -= damage;
    if (data->vital < 0) { data->vital = 0;}
    int currentVital = ( data->vital * 100) / PROPS__PLAYER_VITAL ;
    uiMgr_playerHit(data->uiMgr, currentVital);
}


void player_score(XObject *self, int points) {
    MyPlayer *data = (MyPlayer *)self->custom;
    data->score += points;
    uiMgr_score(data->uiMgr, data->score);
}