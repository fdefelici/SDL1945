#ifndef MY_BEHAV_H
#define MY_BEHAV_H

#include <string.h>
#include <eng.h>
XObject* new_MyBackgr();
XObject* new_MyUIMgr();
XObject* new_MyUIEnergy();
XObject* new_MyUILife(char *name);
XObject* new_MyUIScore();
void uiMgr_playerHit(XObject *uiMgr, unsigned int damage);
void uiMgr_playerDeath(XObject *uiMgr);
void uiMgr_score(XObject *uiMgr, int points);
void uiScore_value(XObject *self, char *);

void player_score(XObject *self, int points);

XObject* new_MyEnemyExplosion(char *name, char *path, int number);

XBehaviour* new_MyPlayerDeathBehav();

#endif
