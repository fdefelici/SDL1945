#ifndef PROPS_H
#define PROPS_H

#define PROPS__BG_SCROLL_SPEED 32 //pixel per secondo
#define PROPS__ENEMY_MOVE_SPEED 64 //pixel per secondo
#define PROPS__SPRITE_TRANSITION_SPEED 16 //pixel per secondo
#define PROPS__PLAYER_MOVE_SPEED 150 //pixel per secondo
#define PROPS__PLAYER_SHOOT_SPEED 300 //pixel per secondo
#define PROPS__PLAYER_VITAL 100
#define PROPS__PLAYER_LIFES 3
#define PROPS__UI_ENERGY_MAX_PIXELS 126

#define PROPS__TRUE 1
#define PROPS__FALSE 0

#define WIN_HEIGHT 480
#define WIN_WIDTH 640

#define PROPS_OBJ_ALL 0
#define PROPS_OBJ_ENABLED 1


#endif