#ifndef XENGINE_H
#define XENGINE_H
#include <aiv/list.h>
#include "SDL.h"
#include "SDL2/SDL_ttf.h"

float clamp(float value, float min, float max);
int xrand(int min, int max);

typedef struct {
    char name[256];
    void (*start)();
    void (*update)();
    void (*destroy)();

    aiv_list_t *childs;

    unsigned int hasRigidBody;
    void (*onCollision)();
    SDL_Rect _rect;
    void* object;
} XEntity;


typedef struct {
    char path[256];
    SDL_Texture *texture;
    unsigned int size;
} XText;

typedef struct {
    char path[256];
    SDL_Texture *texture;
    unsigned int height;
    unsigned int width;
} XImage;

typedef struct {
    XImage base;
    unsigned int width;
    unsigned int height;
    unsigned int number; 
    unsigned int speed;
    unsigned int index; 
    float _pixelCounter; 
    int cycles;
    unsigned int _cycleCounter;
} XSprite;




typedef struct {
    //char name[256];
    void (*start)();
    void (*update)();
    void (*destroy)();
    void (*onCollision)();
} XBehaviour;


typedef struct {
    char name[256];
    struct {
        float x; float y;
        float w; float h;
    } transform;    
    aiv_list_t *children;
    aiv_list_t *behaviours;
    bool enabled;
    bool colliderEnabled;
    void* custom;
} XObject;

typedef struct {
	float deltaTime;
    float fps;
    aiv_list_t *objs;
    SDL_Renderer *renderer;
	int (*run)(void);
} XEngine;

void x_init(XEngine *eng);
int x_run(XEngine *eng);

XObject* new_XObject(char* name);
XBehaviour* new_XBehavior();

void XObject_destroy(XObject *obj);

void XSprite_init(XEngine *eng, XSprite *sprite, char *img_path, unsigned int numberOfImgs);
void XSprite_update(XEngine *eng, XSprite *sprite, XObject *obj);

void XImage_init(XEngine *eng, XImage *image, char *img_path);
void XImage_update(XEngine *eng, XImage *image, XObject *obj);

void XIext_init(XEngine *eng, XText *image, char *text);

XObject* XObject_findByName(XEngine *eng, char* objName);

#endif
