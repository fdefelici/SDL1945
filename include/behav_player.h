#include <eng.h>


typedef struct {
    XSprite sprite;
    XObject *bullet;
    XObject *uiMgr;
    XObject *explosion;
    int vital;
    int lifes;
    int score;
    bool disabled;
    float disabledTimer;
    float blinkTimer;
} MyPlayer;

XObject* new_MyPlayer();